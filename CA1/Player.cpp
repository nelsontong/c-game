#include "Player.h"
#include <iostream>

using namespace std;

Player::Player(int pos)
{
	position = pos;
}

void Player::setPosition(int i)
{
	position = i;
}

void Player::addInventory(string s, string desc)
{
	Object obj;
	bool exists = false;
	for (Object o : inventory)
	{
		if (s == o.getName())
		{
			exists = true;
			cout << "You already picked it up" << endl;
		}
	}
	if (!exists)
	{
		obj.setName(s);
		obj.setDescription(desc);
		inventory.push_back(obj);

		cout << s << " has been addded to your inventory." << endl;
	}
}

int Player::getPosition() const
{
	return position;
}

vector<Object> Player::getInventory() const
{
	return inventory;
}

void Player::printInventory() const
{
	for (Object o : inventory)
	{
		cout << o.getName() << " \t| " << o.getDescription() << endl;
	}
}

Player::~Player()
{

}
