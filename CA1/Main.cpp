#include "Object.h"
#include "Room.h"
#include "Player.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <vector>
#include <string>
#include <iterator>
#include <chrono>

using std::cout;
using std::cin;
using std::endl;
using std::getline;
using std::ifstream;
using std::string;
using std::iterator;
using std::stringstream;
using namespace std::chrono;

const string file = "G:/Computing/Year_3/C++/CA1/rooms.txt";

void readRooms(vector<Room>& rooms);
void showRooms(vector<Room>& rooms);
void addInventory(Player& player, string object, string description);
void showInventory(Player player);
void inspect(Player player, string object);
void welcomeNotice();
string removeFirst(string s);
string getFirstWord(string s);
bool checkInventory(Player player, string s);

void main()
{
	welcomeNotice();
	auto start = steady_clock::now();

	vector<Room> rooms;
	Player player;

	readRooms(rooms);
	//showRooms(rooms);

	bool gameEnd = false;

	cout << endl;
	cout << "You awoke in a place you are unfamiliar with" << endl;
	cout << "You are standing on " << rooms[0].getName() << endl;
	cout << "Description: " << rooms[0].getDescription() << endl;
	cout << endl;

	int roomCount = 0;
	Room*roomPtr = &rooms[0];
	Room*oldRoomPtr = roomPtr;
	string input;
	while (!gameEnd)
	{
		cout << endl;
		getline(cin, input);
		for (int i = 0; i < input.length(); i++)
		{
			input[i] = tolower(input[i]);
		}

		string action = getFirstWord(input);
		string param = removeFirst(input);

		if (input == "end")
		{
			gameEnd = true;
		}
		else if (input == "inventory")
		{
			showInventory(player);
		}
		else if (action == "go")
		{
			if (param == "north")
			{
				int newRoomId = roomPtr->checkExits(0);
				if (newRoomId == -1)
				{
					cout << endl;
					cout << "Cannot go this way" << endl;
					cout << endl;
				}
				else
				{
					for (int i = 0; i < rooms.size(); i++)
					{
						if (rooms[i].getRoomId() == newRoomId)
						{
							roomPtr = &rooms[i];
						}
					}
				}
			}
			else if (param == "south")
			{
				int newRoomId = roomPtr->checkExits(1);
				if (newRoomId == -1)
				{
					cout << endl;
					cout << "Cannot go this way" << endl;
					cout << endl;
				}
				else
				{
					for (int i = 0; i < rooms.size(); i++)
					{
						if (rooms[i].getRoomId() == newRoomId)
						{
							roomPtr = &rooms[i];
						}
					}
				}
			}
			else if (param == "east")
			{
				int newRoomId = roomPtr->checkExits(2);
				if (newRoomId == -1)
				{
					cout << endl;
					cout << "Cannot go this way" << endl;
					cout << endl;
				}
				else
				{
					for (int i = 0; i < rooms.size(); i++)
					{
						if (rooms[i].getRoomId() == newRoomId)
						{
							roomPtr = &rooms[i];
						}
					}
				}
			}
			else if (param == "west")
			{
				int newRoomId = roomPtr->checkExits(3);
				if (newRoomId == -1)
				{
					cout << endl;
					cout << "Cannot go this way" << endl;
					cout << endl;
				}
				else
				{
					for (int i = 0; i < rooms.size(); i++)
					{
						if (rooms[i].getRoomId() == newRoomId)
						{
							roomPtr = &rooms[i];
						}
					}
				}
			}
			else if (param == "up")
			{
				int newRoomId = roomPtr->checkExits(4);
				if (newRoomId == -1)
				{
					cout << endl;
					cout << "Cannot go this way" << endl;
					cout << endl;
				}
				else
				{
					for (int i = 0; i < rooms.size(); i++)
					{
						if (rooms[i].getRoomId() == newRoomId)
						{
							roomPtr = &rooms[i];
						}
					}
				}
			}
			else if (param == "down")
			{
				int newRoomId = roomPtr->checkExits(5);
				if (newRoomId == -1)
				{
					cout << endl;
					cout << "Cannot go this way" << endl;
					cout << endl;
				}
				else
				{
					for (int i = 0; i < rooms.size(); i++)
					{
						if (rooms[i].getRoomId() == newRoomId)
						{
							roomPtr = &rooms[i];
						}
					}
				}
			}
		}
		else if (action == "take") 
		{
			if (roomPtr->getRoomId() == rooms[5].getRoomId())
			{
				bool found = false;
				vector<Object> obj = rooms[5].getObject();
				for (Object o : obj) 
				{
					if (param == o.getName())
					{
						addInventory(player, o.getName(), o.getDescription());
						found = true;
						break;
					}
				}
				if (!found)
				{
					cout << "No such object" << endl;
				}
			}
			else if (roomPtr->getRoomId() == rooms[3].getRoomId())
			{
				bool found = false;
				vector<Object> obj = rooms[3].getObject();
				for (Object o : obj)
				{
					if (param == o.getName())
					{
						addInventory(player, o.getName(), o.getDescription());
						found = true;
						break;
					}
				}
				if (!found)
				{
					cout << "No such object" << endl;
				}
			}
		}
		else if (action == "use")
		{
			if (param == "")
			{
				cout << "Use what?" << endl;
			}
			if (roomPtr->getRoomId() == rooms[8].getRoomId())
			{
				if(checkInventory(player, param))
				{
					auto end = steady_clock::now();
					cout << "Death slowly disappears" << endl;
					gameEnd = true;
					cout << "You Win!!" << endl;
					cout << "Rooms traversed: " << roomCount << endl;
					cout << "Time taken: " << duration_cast<seconds>(end - start).count()
						 << " seconds" << endl;
				}
				else
				{
					cout << "Nothing happened.." << endl;
				}
			
			}
		}
		else if (action == "remove") 
		{
			// create classes for hideDoor extends object (once removed, reveal door(exit*-1 to make positive) 
			// create container extends objects to hold another object
			if (roomPtr->getRoomId() == rooms[2].getRoomId())
			{
				if (param == "rug")
				{
					vector<int> ext = rooms[2].getExits();
				}
			}
		}
		else if (action == "inspect")
		{
			inspect(player, param);
		}
		else if (input == "")
		{
			cout << "Input something" << endl;
		}
		else
		{
			int i;
			i = (rand() % 10) + 1;
			if (i == 1)
			{
				cout << "What?" << endl;
			}
			else if (i == 2)
			{
				cout << "Huh?" << endl;
			}
			else if (i == 3)
			{
				cout << "I don't understand" << endl;
			}
			else if (i == 4)
			{
				cout << "Try something different" << endl;
			}
			else if (i == 5)
			{
				cout << "No" << endl;
			}
			else if (i == 6)
			{
				cout << "Denied" << endl;
			}
			else if (i == 7)
			{
				cout << "Pardon?" << endl;
			}
			else if (i == 8)
			{
				cout << "Nope" << endl;
			}
			else if (i == 9)
			{
				cout << ">:(" << endl;
			}
			else if (i == 10)
			{
				cout << "Try a different input" << endl;
			}
		}

		//don't repeat room name and description after performing an action if still in same room
		if (oldRoomPtr != roomPtr)
		{
			cout << endl;
			cout << roomPtr->getName() << endl;
			cout << roomPtr->getDescription() << endl;
			roomCount += 1;
			cout << endl;
			oldRoomPtr = roomPtr;
		}
	}

	cout << "KTHXBYE :)" << endl;

	//check if roomPtr are the same before deleting
	if (oldRoomPtr == roomPtr)
	{
		delete roomPtr;
		roomPtr = 0;
	}
	else
	{
		delete roomPtr;
		roomPtr = 0;
		delete oldRoomPtr;
		oldRoomPtr = 0;
	}
}

void readRooms(vector<Room>& rooms)
{
	ifstream in(file);
	if (in)
	{
		while (!in.eof())
		{
			Room newRoom;
			bool next = false;
			
			while (!next)
			{
				string line;
				getline(in, line);
				string type = getFirstWord(line);
				
				if (line == "")
				{
					next = true;
				}

				if (type == "room_id:")
				{
					line = (removeFirst(line));
					int roomid = atoi(line.c_str());
					newRoom.setRoomId(roomid);
				}
				else if (type == "room:")
				{
					string name;
					name = removeFirst(line);
					newRoom.setName(name);
				}
				else if (type == "description:")
				{
					string description;
					description = removeFirst(line);
					newRoom.setDescription(description);
				}
				else if (type == "exits:")
				{
					int numExit;
					line = removeFirst(line);
					stringstream ss(line);
					while (!ss.eof())
					{
						ss >> numExit;
						newRoom.setExits(numExit);
					}
				}
				else if (type == "object:")
				{
					string objectName;
					objectName = removeFirst(line);

					string line;
					getline(in, line);
					string type = getFirstWord(line);

					if (type == "obj_description:")
					{
						string objDesc;
						objDesc = removeFirst(line);
						newRoom.addObject(objectName, objDesc);
					}
				}
			}
			rooms.push_back(newRoom);
		}
	}
	else
	{
		cout << "Error opening file" << endl;
	}
}

bool checkInventory(Player player, string s)
{
	vector<Object> inv = player.getInventory();
	for (Object o : inv)
	{
		if (s == o.getName())
		{
			return true;
		}
	}
	return false;
}

string getFirstWord(string s)
{
	int start = s.find(" ");
	if (start != -1)
	{
		return s.substr(0,start);
	}
	return s;
}

string removeFirst(string s)
{
	int start = s.find(" ");
	if (start != -1)
	{
		return s.substr(start + 1);
	}
	return s;
}
void showRooms(vector<Room>& rooms)
{
	for (Room r : rooms)
	{
		r.print();
	}
}

void addInventory(Player& player, string object, string description)
{
	player.addInventory(object, description);
}

void showInventory(Player player)
{
	player.printInventory();
}

void inspect(Player player, string object)
{
	vector<Object> inv = player.getInventory();
	bool found = false;
	for (Object o : inv)
	{
		if (object == o.getName())
		{
			cout << o.getDescription() << endl;
			found = true;
		}
	}
	if (!found)
	{
		cout << "No such object found" << endl;
	}
}

void welcomeNotice()
{
	string::size_type padding = 3;

	string welcomeMsg = "Welcome to DEATH!";

	string::size_type msgLength = welcomeMsg.size();

	string blank(msgLength + (padding * 2), ' ');
	string blankLine = "*" + blank + "*";
	string top(msgLength + (padding * 2) + 2, '*');
	string paddingLeft(padding, ' ');

	cout << top << endl;
	for (int i = 0; i < padding / 2; i++)
	{
		cout << blankLine << endl;
	}
	cout << "*" << paddingLeft << welcomeMsg << paddingLeft << "*" << endl;
	for (int i = 0; i < padding / 2; i++)
	{
		cout << blankLine << endl;
	}
	cout << top << endl;


	cout << "Use key words to navigate through the game such as go north, "
	"go east, take, remove, break. To exit the game enter 'end'" << endl;
}