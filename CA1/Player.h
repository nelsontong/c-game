#pragma once
#include "Object.h"
#include <string>
#include <vector>

using std::vector;
using std::string;

class Player
{
private:
	int position;
	vector<Object> inventory;

public:
	Player(int position = 0);

	void setPosition(int i);
	void addInventory(string s, string description);

	int getPosition() const;
	vector<Object> getInventory() const;

	void printInventory() const;

	~Player();
};

