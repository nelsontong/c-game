#pragma once
#include <iostream>
#include <string>
#include <vector>
#include "Object.h"

using std::string;
using std::vector;

class Room
{
private:
	int roomId;
	string name;
	string description;
	vector<int> exits;
	vector<Object> objects;
	
public:
	Room();

	void setRoomId(int i);
	void setName(string s);
	void setDescription(string s);
	void setExits(int i);
	void addObject(string name, string description);

	int getRoomId() const;
	string getName() const;
	string getDescription() const;
	vector<int> getExits() const;
	vector<Object> getObject() const;
	int checkExits(int i) const;

	void print() const;

	~Room();
};

