#pragma once
#include <iostream>
#include <string>

using std::string;

class Object
{
private:
	string name;
	string description;

public:
	Object(string name = "", string description = "");
	void setName(string s);
	void setDescription(string s);

	string getName() const;
	string getDescription() const;

	~Object();
};

