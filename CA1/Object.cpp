#include "Object.h"

Object::Object(string nm, string dsc)
{
	name = nm;
	description = dsc;
}

void Object::setName(string s)
{
	name = s;
}
void Object::setDescription(string s)
{
	description = s;
}

string Object::getName() const
{
	return name;
}
string Object::getDescription() const
{
	return description;
}

Object::~Object()
{

}
