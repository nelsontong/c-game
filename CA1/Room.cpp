#include "Room.h"
#include "Object.h"
#include <iostream>
#include <string>

using std::cout;
using std::endl;
using std::string;

Room::Room()
{

}

void Room::setRoomId(int i)
{
	roomId = i;
}

void Room::setName(string s)
{
	name = s;
}

void Room::setDescription(string s)
{
	description = s;
}

void Room::setExits(int i)
{
	exits.push_back(i);
}

int Room::getRoomId() const
{
	return roomId;
}

string Room::getName() const
{
	return name;
}

string Room::getDescription() const
{
	return description;
}

vector<int> Room::getExits() const
{
	return exits;
}

vector<Object> Room::getObject() const
{
	return objects;
}

void Room::addObject(string name, string description)
{
	Object obj;
	obj.setName(name);
	obj.setDescription(description);
	objects.push_back(obj);
}

int Room::checkExits(int direction) const
{
	return exits[direction];
}

void Room::print() const
{
	cout << "Room ID : " << roomId << endl;
	cout << "Room Name : " << name << endl;
	cout << "Description : " << description << endl;
	cout << "Exits : ";
	for (int i : exits)
	{
		cout << i << " " ;
	}

	cout << endl;
	
	for (Object o : objects)
	{
		cout << "Object : " << o.getName() << endl;
		cout << "Object description : " << o.getDescription() << endl;
	}
	
	cout << "\n" << endl;
	
}

Room::~Room()
{

}
